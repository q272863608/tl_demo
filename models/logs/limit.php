<?php
class mdl_logs_limit extends ujn_model {
		
	const COUNT_LOGIN_CHIPS				= 'count_login_chips';						//连续登陆奖励
	
	/**
	 * 防刷新限制.当天限制，一天只能做一次
	 * Enter description here ...
	 */
	public function limitOnceDaily($uid, $ltype, $onlyQuery = false) {
		$flag = false;
		if(! ($uid = tls_func::uint( $uid ) ) || ! strlen( $ltype ) ){
			return  $flag;
		}
		$cacheKey = mdl_ckey::getck(mdl_ckey::M_SYSTEM_LOG, $uid, $ltype);
	
		if($onlyQuery) {
			$flag = ($this->mc('limit')->get($cacheKey) == 1) ? true : false;
		}else {
			$flag = $this->mc("limit")->add($cacheKey, 1, ( TODAY + 86400 ) - NOW);
		}
		return $flag;
	}

	/**
	 * 防刷新限制.当天限制，一天只能限制做X次
	 * return
	 *      false 已经超出限制
	 *      true  还没超出限制
	 */
	public function limitDaily($uid, $ltype, $limitNum, $onlyQuery = false, $num = 1) {
		$flag = false;
		if(! ($uid = tls_func::uint($uid)) || ! strlen($ltype)){
			return  $flag;
		}
		$limitNum = tls_func::uint($limitNum);

		$cacheKey = mdl_ckey::getck(mdl_ckey::COMMON_LIMIT_DAILY, $uid, $ltype);
		if($onlyQuery) {
			$flag = ($this->mc('limit')->get($cacheKey) >= $limitNum) ? false : true;
		}else {
			$flag = ($this->mc("limit")->increment($cacheKey, $num) > $limitNum) ? false : true;
		}

		return $flag;
	}
	
	/**
	 * 一周只能限制做1次
	 */
	public function limitOnceWeekly($uid, $ltype, $onlyQuery = false) {
		$flag = false;
		if(! ($uid = tls_func::uint( $uid ) ) || ! strlen( $ltype ) ){
			return  $flag;
		}
		$w = date('w');
		if(date('w') == 0) {
			$w = 7;
		}
		$w = $w-1;
		
		$cacheKey = mdl_ckey::getck(mdl_ckey::COMMON_LIMIT_WEEKLY, $uid, $ltype);

		if($onlyQuery) {
			$flag = ($this->mc('limit')->get($cacheKey) == 1) ? true : false;
		}else {
			$flag = $this->mc("limit")->add($cacheKey, 1, 86400 * 7 - ($w * 86400 + (NOW - TODAY)));
		}
		return $flag;
	}

	public function getLimitCount($uid, $ltype) {
		$count = 0;
		if(!$uid = tls_func::uint($uid)) return $count;
		 
		$cacheKey = mdl_ckey::getck(mdl_ckey::COMMON_LIMIT_DAILY, $uid, $ltype);
		$count = $this->mc('limit')->get($cacheKey);
		 
		return intval($count);
	}

	public function initLimitDaily($uid, $ltype, $count=0) {
		$flag = false;
		if(! ($uid = tls_func::uint($uid)) || ! strlen($ltype)){
			return  $flag;
		}
		$count = tls_func::uint($count);

		$cacheKey = mdl_ckey::getck(mdl_ckey::COMMON_LIMIT_DAILY, $uid, $ltype);

		$flag = $this->mc("limit")->set($cacheKey, $count, ( TODAY + 86400 ) - NOW);

		return $flag;
	}

	/**
	 * 防刷新限制.永久限制，暂时认为10天为永久，永久只能做一次
	 * Enter description here ...
	 */
	public function limitOnceForever($uid, $ltype, $onlyQuery = false) {
		$flag = false;
		if(! ($uid = tls_func::uint( $uid ) ) || ! strlen( $ltype ) ){
			return  $flag;
		}
		$cacheKey = mdl_ckey::getck(mdl_ckey::COMMON_LIMIT_FOREVER, $uid, $ltype);
		if($onlyQuery) {
			$flag = ($this->mc('limit')->get($cacheKey) == 1) ? true : false;
		}else {
			$flag = $this->mc("limit")->add($cacheKey, 1, ( TODAY + 86400*10 ) - NOW);
		}

		return $flag;
	}
	
	/**
	 * 防刷新限制.永久限制，暂时认为10天为永久，永久只能做一次
	 * Enter description here ...
	 */
	public function systemLimitOnceForever($uid, $ltype, $number, $onlyQuery = false) {
		$flag = false;
		if(! ($uid = tls_func::uint( $uid ) ) || ! strlen( $ltype ) ){
			return  $flag;
		}
		$cacheKey = mdl_ckey::getck(mdl_ckey::COMMON_LIMIT_FOREVER, $uid, $ltype);
		if($onlyQuery) {
			$flag = ($this->mc('limit')->get($cacheKey) == 1) ? true : false;
		}else {
			$flag = $this->mc("limit")->increment($cacheKey, 1, ( TODAY + 86400*10 ) - NOW, 0) > $number ? false : true;
		}

		return $flag;
	}
	
	public function lock($uid, $ltype) {
		$flag = false;
		if(!($uid = tls_func::uint($uid)) || !strlen($ltype)) {
			return  $flag;
		}
		$cacheKey = mdl_ckey::getck(mdl_ckey::COMMON_LOCK, $uid, $ltype);
		$flag = $this->mc("limit")->add($cacheKey, 1, 60);

		return $flag;
	}
	
	public function unlock($uid, $ltype) {
		$flag = false;
		if(!($uid = tls_func::uint($uid)) || !strlen($ltype)) {
			return  $flag;
		}

		return $this->mc("limit")->delete(mdl_ckey::getck(mdl_ckey::COMMON_LOCK, $uid, $ltype));
	}

	protected function __construct() {
		parent::__construct();
	}

	/**
	 * 单例模型
	 * @return mdl_logs_limit
	 */
	static function getInstance() {
		return parent::getInstance(__CLASS__);
	}
}