<?php
class mdl_logs_chips extends ujn_model {
	const FLAG_DESCR							= 0;
	const FLAG_ADD								= 1;

	const REGISTER_REWARD						= 101;			// 註冊獎勵
	const CONT_LOGIN							= 401;			//连续登陆奖励
	
	const PAY_CHIPS								= 201;  		// 支付 直冲金币

	public static $chips = array( //基准值  增
		mdl_logs_chips::REGISTER_REWARD 			=> array(2000, 0)
	);

	/**
	 *
	 * 设置动态 $update: 是否执行更新操作 $lflag:0加1减
	 * @param $mid
	 * @param chips来源 $lmode
	 * @param 1加0减 $lflag
	 * @param 输赢数目 $lchips
	 * @param 环境ID,如游戏桌ID,反馈ID,礼物ID $lremark
	 * @param 附加信息:比如邀请好友的好友的sitemid,便于页面显示 $ldesc
	 * @param 设为1则真实加钱, 否则仅添加日志, 只接受1或者0, 其他任何都认为是0 $update
	 */
	function addWin($uid, $lmode, $lflag, $lchips, $lremark, $ldesc = '', $update = 0) {
		if( ! $uid = tls_func::uint( $uid ) ) {
			return false;
		}

		$lmode		= tls_func::uint( $lmode );
		$lflag		= tls_func::uint( $lflag );
		$lchips		= tls_func::uint( $lchips );
		$update		= tls_func::uint( $update );
		$lremark	= tls_func::escape( $lremark );
		$ldesc		= tls_func::escape( $ldesc );
		$ltime		= NOW;

		// base::p($update);echo '<br/>';
		// base::p($lchips);die;
		$flag = false;
		$update && $lchips && $flag = mdl_member_info::getInstance()->addMoney($uid, $lchips, $lflag); //需要真实地更新钱数
		if( !$update || ( $update && $flag ) ) {

			$query = "INSERT DELAYED INTO {$this->tbl_name} SET mid='$uid', lmode='$lmode', lflag='$lflag', lchips='$lchips', lremark='$lremark',ldesc='$ldesc', ltime='$ltime'";
			// return  $query;
			$this->_db->query($query);
			return $this->_db->affectedRows();
		}
	}

	protected function __construct() {
		parent::__construct();
		$this->db_name		= ujn::getCFG("dhs.default.db_name");
		$this->tbl_name		= "`{$this->db_name}`.`logs_chips`";
		$this->_db			= $this->mysql();
	}

	/**
	 * 单例模型
	 * @return mdl_logs_chips
	 */
	static function getInstance() {
		return parent::getInstance(__CLASS__);
	}
}