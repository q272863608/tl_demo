<?php
class mdl_ckey {
	const M_PUID_TO_UID         			= 'm_puid_to_uid'; //userInfo
	const M_INFO_KEY_UID        			= 'm_info_key_uid'; //userInfo
	
	const M_MTKEY_UID          				= 'm_mtkey_uid';
	const SYSTEM_STAT_REGISTER  			= 'system_stat_register'; //用户注册统计
	const M_SYSTEM_SERVERS					= 'm_system_servers';
	const M_SYSTEM_ROOMS					= 'm_system_rooms';
	
	const M_SYSTEM_LOG						= 'm_system_log';

	static function getck($ck, $uid, $pre = '') {
		switch(strval($ck)) {	
			case mdl_ckey::M_PUID_TO_UID:
				return "{$ck}.{$pre}.{$uid}";
				break;

			case mdl_ckey::M_INFO_KEY_UID;
			case mdl_ckey::M_MTKEY_UID;
			case mdl_ckey::M_SYSTEM_LOG:
				return "{$ck}.{$uid}";
				break;

			case mdl_ckey::SYSTEM_STAT_REGISTER:
				return "{$ck}.{$pre}";
				break;
		}
	}
}