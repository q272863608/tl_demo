<?php
class mdl_member_info extends ujn_model {
	
	public function calculateSeriesTimes($lastActiveTime, $series_times) {
		//判断昨天是否登陆过
		$isSeries = (TODAY - $lastActiveTime) > 86400 ? false : true;
		if($isSeries) {
			$series_times += 1;
		}else {
			$series_times = 1;
		}
		return $series_times;
	}
	
	public function getUser($puid, $site, $useCache = false, $onlyUid = false) {
		$site = tls_func::escape($site);
		$puid = tls_func::escape($puid);

		$m = array();

		$uid = $this->mc("minfo")->get(mdl_ckey::getck(mdl_ckey::M_PUID_TO_UID, $puid, $site));
	
		if(!$uid) {
			$tmp = $this->_db->getOne("select mid from {$this->tbl_info} where sitemid = '{$puid}' and site = '{$site}' limit 1;");
			
			$uid = !empty($tmp) ? $tmp['mid'] : 0;
		}

		if($onlyUid) return $uid;
		
		if($uid) {
			$m = $this->getUserByUID($uid, $useCache);
			$this->mc("minfo")->set(mdl_ckey::getck(mdl_ckey::M_PUID_TO_UID, $puid, $site), $uid);
		}
		return $m;
	}

	public function getUserInfo($mid) {
		$mid = tls_func::escape($mid);
		if(!empty($mid)) $m = $this->_db->getOne("select `mid`,`sitemid`,`site`,`name`,`sex` from {$this->tbl_info} where `mid` = '{$mid}' limit 1;");
		return $m;
	}

	public function getUserByUID($uid, $useCache = false) {
		$m = array();
		if(!$uid = tls_func::uint($uid)) return $m;

		$cacheKey = mdl_ckey::getck(mdl_ckey::M_INFO_KEY_UID, $uid);

		if($useCache) {
			$m = $this->mc("minfo")->get($cacheKey);
		}

		if(!$useCache || empty($m)) {
			$i = $this->_db->getOne("select * from {$this->tbl_info} where mid = '{$uid}' limit 1;");
			$m = $this->_db->getOne("select * from {$this->tbl_game} where mid = '{$uid}' limit 1;");
			$m = array_merge($i, $m);
			!empty($m) && $this->mc("minfo")->set($cacheKey, $m);
		}
		return $m;
	}

	public function saveUser($uinfo) {
		$u = array();
		$u['sitemid']   	= tls_func::escape($uinfo['sitemid']);
		$u['site']   		= tls_func::escape($uinfo['site']);
		$u['name'] 			= tls_func::escape($uinfo['name']);
		$u['pwd']			=tls_func::escape($uinfo['pwd']);
		$u['big_pic']   	= isset($uinfo['big_pic']) ? $uinfo['big_pic'] : '';
		$u['sex']       	= $uinfo['sex'];
		$u['series_times']	= 1;

		$u['chips'] = mdl_logs_chips::$chips[mdl_logs_chips::REGISTER_REWARD][0];
		
		$sql="insert into {$this->tbl_info} set `sitemid`='{$u['sitemid']}',`pwd`='{$u['pwd']}',`site`='{$u['site']}', 
				`name`='{$u['name']}',`sex`='{$u['sex']}',`big_pic` ='{$u['big_pic']}', `series_times`='{$u['series_times']}', `mactived`='".NOW."', `mcreated`='" . NOW . "'";
		
		$this->_db->query($sql);
		$u['mid']      = $this->_db->insertID();
		
		$this->_db->query("insert into {$this->tbl_game} set `mid`='{$u['mid']}', `chips`='{$u['chips']}';");
		
		$u['exp']      = 0;
		$u['mcreated'] = NOW;
		
		mdl_logs_chips::getInstance()->addWin($u['mid'], mdl_logs_chips::REGISTER_REWARD, mdl_logs_chips::FLAG_ADD, $u['chips'], '');

		return $u;
	}
	
	public function update($mid, $data) {
		if(!$mid = tls_func::uint($mid)) return false;
		
		$allows = array('name', 'mactived', 'series_times','sex');
		$saves = array();
		foreach((array)$data as $k => $v) {
			if(in_array($k, $allows, true)) {
				$saves[] = "`{$k}`='".tls_func::escape($v)."'";
			}else if($k == 'login_times') {
				$saves[] = "`{$k}`=`{$k}` + ".tls_func::escape($v)."";
			}
		}
		if(empty($saves)) return false;

		$sql="update {$this->tbl_info} set ".implode(',', $saves)." where mid = '{$mid}';";
		$flag = $this->_db->query($sql);
		$flag && $this->mc("minfo")->delete(mdl_ckey::getck(mdl_ckey::M_INFO_KEY_UID, $mid));

		return $flag; 
	}


	//修改用户筹码
	public function addMoney($uid, $chips, $sflag) {
		$uid = tls_func::uint($uid);
		$chips = tls_func::uint($chips);

		if ( $uid < 1 || $chips < 1 ) {
			return false;
		}

		$chips = $sflag == 1 ? $chips : -1*$chips;

		if($sflag == 1) { //加钱操作, 限制单次最大加钱数目
			$chips = min(5000000, $chips);
		}
		$query = "UPDATE {$this->tbl_game} SET `chips` = `chips` + '$chips' WHERE mid = '$uid' AND `chips` + '$chips' >= 0";
		$this->_db->query( $query );
		return $this->_db->affectedRows();
	}

	public function get_mtkey($uid) {
		$cacheKey = mdl_ckey::getck(mdl_ckey::M_MTKEY_UID, $uid);
		$_mtkey = md5(microtime(true) . $uid);
		$this->mc("servers")->set($cacheKey, $_mtkey, 86400);
	    return $_mtkey;
	}
	
	function get_uid_by_mtkey($mtkey, $uid) {
		if(empty($mtkey)) return false;
		$cacheKey = mdl_ckey::getck(mdl_ckey::M_MTKEY_UID, $uid);
		$_mtkey = $this->mc("servers")->get($cacheKey);
		return $mtkey === $_mtkey ? $uid : false;
	}

	protected function __construct() {
		parent::__construct();
		$this->db_name		= ujn::getCFG("dhs.default.db_name");
		$this->tbl_game		= "`{$this->db_name}`.`member_game_info`";
		$this->tbl_info		= "`{$this->db_name}`.`member_profile_info`";
		$this->_db			= $this->mysql();
	}
	
	/**
	 * 
	 * @return mdl_member_info
	 */
	static function getInstance() {
		return parent::getInstance(__CLASS__);
	}
}