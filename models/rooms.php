<?php
class mdl_rooms extends ujn_model {

	public function getRoomsInfo() {
		$rooms=$this->mc("minfo")->get(mdl_ckey::M_SYSTEM_ROOMS);
		if($rooms===false) {
			$roomsql = "SELECT * FROM $this->tbl_rooms";
			$rooms = $this->_db->getAll($roomsql);
			$this->mc("minfo")->delete(mdl_ckey::M_SYSTEM_ROOMS);
			$this->mc("minfo")->set(mdl_ckey::M_SYSTEM_ROOMS,$rooms,86400*7);
		}
			return  $rooms;
	}
	
	protected function __construct() {
		parent::__construct();
		$this->db_name		= ujn::getCFG("dhs.default.db_name");
		$this->tbl_rooms	= "`{$this->db_name}`.`rooms`";
		$this->_db			= $this->mysql();

		
	}
	
	/**
	 * 
	 * @return add_info
	 */
	static function getInstance() {
		return parent::getInstance(__CLASS__);
	}
}