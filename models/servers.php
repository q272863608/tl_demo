<?php
class mdl_servers extends ujn_model{
	public function getServerInfo(){
			$str ='';
			$servers = $this->mc("minfo")->get(mdl_ckey::M_SYSTEM_SERVERS);
			if($servers === false){
				$serverssql = "SELECT * FROM $this->tbl_rooms_servers";
				$servers = $this->_db->getAll($serverssql);
				$this->mc("minfo")->delete(mdl_ckey::M_SYSTEM_SERVERS);
				$this->mc("minfo")->set(mdl_ckey::M_SYSTEM_SERVERS,$servers,86400*7);
			}
				return $servers;
		}
		
		protected function __construct() {
			parent::__construct();
			$this->db_name		= ujn::getCFG("dhs.default.db_name");
			$this->tbl_rooms_servers = "`{$this->db_name}`.`rooms_servers`";
			$this->_db			= $this->mysql();
		}
		
		/**
		 * 
		 * @return add_info
		 */
		static function getInstance() {
			return parent::getInstance(__CLASS__);
		}
}