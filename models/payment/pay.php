<?php
class mdl_payment_pay extends ujn_model {
	const STATUS_SELF_PLACE_ORDER        = 0;
	const STATUS_THIRD_PLACE_ORDER       = 1;
	const STATUS_COMPLETE_ORDER          = 2; //完成
	const STATUS_CANCEL_ORDER            = 3; //取消
	
	/*
	 * 下单
	 * 返回定单号
	 */
	public function place_order($uid, $product, $pmode, $price, $chips, $current_chips) {
		if(!$uid = tls_func::uint($uid)) return false;
		$product = tls_func::uint($product);
		$pmode = tls_func::uint($pmode);
		// $price = tls_func::uint($price);
		$chips = tls_func::uint($chips);
		$current_chips = tls_func::uint($current_chips);
		
		$sql = "insert into {$this->tbl_name} set `mid`='{$uid}', `product`='{$product}', 
		          `pmode`='{$pmode}', `price`='{$price}', `chips`='{$chips}', `current_chips`='{$current_chips}', `place_date`='" . NOW . "'";
		$this->_db->query($sql);
    return $this->_db->insertID();
	}
	
	/*
	 * 通过定单号修改定单状态
	 * 返回影响的行数
	 * @param $pid	定单号
	 * @parma $status 定单状态
	 */
	public function change_update($pid, $status) {
		if(!$pid = tls_func::uint($pid)) return false;
		$status = tls_func::uint($status);
		
		$sql = "update {$this->tbl_name} set `status`='{$status}' where `pid`='{$pid}' and `status`<'" . mdl_payment_pay::STATUS_COMPLETE_ORDER . "'";
		$this->_db->query($sql);
		
		return $this->_db->affectedRows();
	}
	
	/*
	 * @param $pid 定单号
	 * @param $trand_id 第三方支付平台的定单号
	 */
	public function complete($pid, $trans_id) {
		$pid =mdl_payment_query::getInstance()->getfirstPid($pid);
		$flag = false;
		if(!$pid = tls_func::uint($pid)) return $flag;

		$order = mdl_payment_query::getInstance()->get_not_complete($pid); //得到当前定单的详细信息
		if(!$order) return false;
		$trans_id = tls_func::escape($trans_id);
		$sql = "update {$this->tbl_name} set `trans_id`='{$trans_id}', 
		         `status`='" . mdl_payment_pay::STATUS_COMPLETE_ORDER . "', `complete_date`='" . NOW . "' where `pid` = '{$pid}'";
		$this->_db->query($sql);
	
		if($this->_db->affectedRows()) {
			$flag = mdl_logs_chips::getInstance()->addWin($order['mid'], mdl_logs_chips::PAY_CHIPS, mdl_logs_chips::FLAG_ADD, $order['chips'], $order['pid'], '', 1);       
		}
		
		return $flag;
	}

	public function getOrderFrom($pid){
		$pid=$pid =mdl_payment_query::getInstance()->getfirstPid($pid);
		$sql="select `price` from $this->tbl_name where `pid`='{$pid}'";
		return $this->_db->getOne($sql);
	}
	
	//赠送筹码
	public function give_log($pid, $chips) {
		if(!$pid = tls_func::uint($pid)) return false;
		$chips = tls_func::uint($chips);
		
		//通过交易id,和状态码查找用户信息
		$order = mdl_payment_query::getInstance()->get($pid, mdl_payment_pay::STATUS_COMPLETE_ORDER);
		if(!$order) return false;
		
		$sql = "update {$this->tbl_name} set `give_chips`='{$chips}' where `pid` = '{$pid}'";
		$this->_db->query($sql);
		
		return $this->_db->affectedRows();
	}

	public function writelogs($arr) {
		$str=var_export($arr,true);
		$filePath = PATH_TMP . 'pay_test_1.log';
		$str=$str.date("Y-m-d H:i:s",NOW);
		file_put_contents($filePath, $str, FILE_APPEND);
	}

  protected function __construct() {
    parent::__construct();
    $this->db_name    = ujn::getCFG("dhs.default.db_name");
    $this->tbl_name   = "`{$this->db_name}`.`payment`";
    $this->_db      = $this->mysql();
  }

  /**
   * 单例模型
   * @return mdl_payment_pay
   */
  static function getInstance() {
    return parent::getInstance(__CLASS__);
  } 
}