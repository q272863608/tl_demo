<?php
class mdl_payment_query extends ujn_model {
	public function get_not_complete($pid) {
		$data = array();
		if(!$pid = tls_func::uint($pid)) return $data;
		$sql = "select * from {$this->tbl_name} where `pid`='{$pid}' and `status`< '". mdl_payment_pay::STATUS_COMPLETE_ORDER ."' limit 1";
		return $this->_db->getOne($sql);
	}
		
	public function get($pid, $status = 0) {
		$data = array();
		if(!$pid = tls_func::uint($pid)) return $data;
		 
		$sql = "select * from {$this->tbl_name} where `pid`='{$pid}' and `status` = '{$status}' limit 1";
		return $this->_db->getOne($sql);
	}

	/*
	 * 统计指定用户成功的交易记录
	 * 返回一个整数
	 */
	public function getCompleteCount($mid) {
		$count = 0;
		if(!$mid = tls_func::uint($mid)) return $count;
		 
		$sql = "select count(`pid`) as count from {$this->tbl_name} where `mid`='{$mid}' and `status` = ". mdl_payment_pay::STATUS_COMPLETE_ORDER;
		$temp = $this->_db->getOne($sql);
		if($temp) $count = $temp['count'];

		return $count;
	}

	public function gets($uid, $limit=15) {
		$data = array();
		if(!$uid = tls_func::uint($uid)) return $data;
		$limit = tls_func::uint($limit);

		$sql = "select * from {$this->tbl_name} where `mid`='{$uid}' order by pid desc limit {$limit}";
		return $this->_db->getAll($sql);
	}

	public function getExternalPid($pid) {
		 return 'ttl' . $pid;
	}

	public function getfirstPid($pid){
		$pid=ltrim($pid,'ttl');
		return tls_func::uint($pid);
	}
	protected function __construct() {
		parent::__construct();
		$this->db_name    = ujn::getCFG("dhs.default.db_name");
		$this->tbl_name   = "`{$this->db_name}`.`payment`";
		$this->_db      = $this->mysql();
	}

	/**
	 * 单例模型
	 * @return mdl_payment_query
	 */
	static function getInstance() {
		return parent::getInstance(__CLASS__);
	}
}