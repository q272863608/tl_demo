<?php 
	class mdl_message extends ujn_model{
		const SUCCESS 					= 0; 	//成功
		const UNKNOWN					= 1; 	//未知错误
		const READ_CONFIG_ERROR			= 2;	//读取配置文件错误

		const DB_WRITE_ERROR 			= 3;	//写入数据库失败
		const PAY_SIGNATURE_ERROR		= 4; 	//签名有误
		const PAY_PRICE_ERROR			= 5;    //单价不一致
		const AUTH_PWD_ERROR			= 801;	//用户密码不一致(错误)
		const AUTH_USER_ERROR			= 802;	//用户名错误(没有这个用户)
		const AUTH_PARAM_EMPTY			= 803;	//接收数据为空(参数不正确)
		const AUTH_REPEAT_REGISTER 		= 804;   //重复注册
		const AUTH_ALREADY_REWARD		= 805;	//已经奖励过	
		const AUTH_MTKEY_ERROR			= 806;	//mtkey验证错误
		const AUTH_MTKEY_UID_ERROR		= 807;	//mtkey验证用户ID错误		

		static function error($code, $data = null) {
			$msg['code'] = $code;
			if(!is_null($data)) $msg['data'] = $data;
			die(json_encode($msg));
		}

		static function success($code, $data = null) {
			$msg['code'] = $code;
			if(!is_null($data)) $msg['data'] = $data;
			die(json_encode($msg));
		}
	}
?>