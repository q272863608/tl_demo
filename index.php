<?php
include_once './common.php';

	$cfg = array();
	$cfg['dhs']						= require_once PATH_TMP . 'dhs.php';
	$cfg['controllers_path']		= PATH_POINTS;
	$cfg['models_path']				= PATH_MODELS;
	$cfg['templates_path']			= PATH_TEMPLATE;
	$cfg['tmp_path']            	= PATH_TMP;
	// $cfg['lang']					= require_once PATH_TMP . 'lang.php';
	// $cfg['system']              = require_once PATH_CONFIG . 'system.php';
	// $tmp_system					= require_once PATH_TMP . 'system.php';
	// $cfg['system'] 				= array_merge($cfg['system'], $tmp_system);
	$_REQUEST['ctl']= empty($_REQUEST['ctl']) ? 'login' : $_REQUEST['ctl'];
	$_REQUEST['act']= empty($_REQUEST['act']) ? 'index' :  $_REQUEST['act'];
	ujn::run($cfg);
?>