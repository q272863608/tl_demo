<?php 
/**
 * 获取房间信息
 */
class serverinfo extends ujn_controller {
	var $auto_render = false;
	function get() {
		$rooms = mdl_rooms::getInstance()->getRoomsInfo();
		$servers = mdl_servers::getInstance()->getServerInfo();
		$payments = require PATH_ROOT.'_tmp/product_conf.php';
		if(!$rooms) mdl_message::error(mdl_message::UNKNOWN);
		if(!$servers) mdl_message::error(mdl_message::UNKNOWN);
		$pay = array();
		foreach ($payments['price'] as $key => $value) {
			$pay[] = array('price' => $key,'chips' => $value);
		}
		$data = array(
			'rooms' => $rooms,
			'servers' => $servers,
			'payments' => $pay
		);
		
		mdl_message::success(mdl_message::SUCCESS, $data);
	}
}
 ?>