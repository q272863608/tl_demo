<?php 
class user extends ujn_controller {
	var $auto_render = false;

	//用户注册/登陆
	public function init() {
		if(!empty($_REQUEST)) {
			$site = tls_func::escape($_REQUEST['channel']);  //渠道
			$sitemid = tls_func::escape($_REQUEST['openUDID']);	//渠道iD/机器码
			$name = tls_func::escape($_REQUEST['name']);	//用户名
			$pwd = tls_func::escape($_REQUEST['pwd']);	//密码
			//没有填写用户名和密码
			if(empty($site) || empty($sitemid) || empty($name) || empty($pwd)) {
				mdl_message::error(mdl_message::AUTH_PARAM_EMPTY);
			}
			$user = mdl_member_info::getInstance()->getUser($sitemid, $site); //只能判断用户是否存在,返回用户的所有数据

			if(!empty($user)) {
				$state = mdl_logs_limit::getInstance()->limitOnceDaily($user['mid'], mdl_logs_limit::COUNT_LOGIN_CHIPS, true);
				/**
				 * 0 标识领取过
				 * 1 标示还未领取今天的登陆奖励
				 */
				$user['reward'] = $state ? 0 : 1;
				if(md5($pwd) != $user['pwd']) mdl_message::error(mdl_message::AUTH_PWD_ERROR); //验证密码

				$user['mtkey'] = mdl_member_info::getInstance()->get_mtkey($user['mid']);
				$user['status'] = '';
				//判断是否奖励
				if($user['mactived'] < TODAY) {
					$user['status'] = 'login';
					$user = $this->_login($user);
				}
				$user['payments'] = $this->rePayments($site);
				mdl_message::success(mdl_message::SUCCESS, $user);
			} else {
				//用户不存在,注册
				$data = $this->_register($sitemid, $site, $name, $pwd);
				$data['mtkey'] = mdl_member_info::getInstance()->get_mtkey($data['mid']);
				$data['reward'] = 1;
				$data['status'] = 'register';
				$data['payments'] = $this->rePayments($site);
				ujn_model::getInstance('ujn_model')->mc('stat')->append(mdl_ckey::getck(mdl_ckey::SYSTEM_STAT_REGISTER, 0, date("j")), $data['mid'] . ',', 20*86400);
				mdl_message::success(mdl_message::SUCCESS, $data);
			}
		}
	}

	//返回商品信息
	public function rePayments($site) {
		$payments = require PATH_ROOT.'_tmp/product_conf.php';
		$sites = $payments['price'];
		$product =  isset($sites["$site"]) ? $sites["$site"] : $sites['default'];
		return $product;
	}

	private function _register($sitemid, $site, $name, $pwd) {
		$data=array(
					'sitemid'	=> $sitemid,
					'site'		=> $site,
					'pwd'		=> md5($pwd),
					'name'		=> $name,
					'sex'		=> 0,
					'big_pic'	=> '',
					'win_count' => '0',
					'loss_count'=> '0',
					);
		return mdl_member_info::getInstance()->saveUser($data);
	}

	private function _login($user) {
		$series_times=mdl_member_info::getInstance()->calculateSeriesTimes($user['mactived'], $user['series_times']);
		$data = array(
			'series_times'	=>	$series_times,
			'login_times'	=> 	1,
			'mactived'		=> 	NOW
		);

		mdl_member_info::getInstance()->update($user['mid'], $data);
		$user['login_times'] = $user['login_times'] + 1;
		$user = array_merge($user, $data);
		return $user;
	}

	//查询用户信息
	function get(){
		$mid =! empty($_REQUEST['mid']) ? $_REQUEST['mid'] : '';
		$mid = tls_func::uint($mid);

		$data = mdl_member_info::getInstance()->getUserByUID($mid);
			if($data) {
				mdl_message::success(mdl_message::SUCCESS, $data);
			} else {
				//用户不存在
				mdl_message::error(mdl_message::AUTH_USER_ERROR, 'user_notexist');
			}
	}

	public function update(){
		$mid = tls_func::uint($_REQUEST['mid']);
		$name = !empty($_REQUEST['name']) ? $_REQUEST['name'] : mdl_message::error(mdl_message::AUTH_USER_ERROR, 'user_notexist');
		$sex = !empty($_REQUEST['sex']) ? $_REQUEST['sex'] : '0';
		$data = array();
		$data['name'] = tls_func::escape($name);
		$data['sex'] = tls_func::uint($sex);

		$mtkey = !empty($_REQUEST['s']) ? tls_func::escape($_REQUEST['s']) : 0;
		$mtkey = mdl_member_info::getInstance()->get_uid_by_mtkey($mtkey, $mid);
		if(!$mtkey) return mdl_message::error(mdl_message::AUTH_MTKEY_ERROR, 'mtkcode_error');

		if(empty($mid) || empty($name)){
			mdl_message::error(mdl_message::AUTH_USER_ERROR, 'user_notexist');
		}
		$flag = mdl_member_info::getInstance()->update($mid, $data);
		$data = mdl_member_info::getInstance()->getUserInfo($mid);
		mdl_message::success(mdl_message::SUCCESS, $data);
	}
}