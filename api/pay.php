<?php
class pay extends ujn_controller{
	var $auto_render = false;
	var $payments=array();
	function __construct(){
		$this->payments=require PATH_ROOT.'_tmp/product_conf.php';
	}
	
	/**
	 * 下单
	 * 要用户ID,product(产品),price(价格),pmode(支付方式)
     */
	public function single() {
		$mid = tls_func::uint($_REQUEST['mid']);
		$pmode = $this->payments['pmode'][$_REQUEST['pmode']];
		$product = $_REQUEST['product'];
		$price = $_REQUEST['price'];

		$userInfo = mdl_member_info::getInstance()->getUserByUID($mid);
		if(!$userInfo) mdl_message::error(mdl_message::AUTH_USER_ERROR); //没有这用户

		$site = $userInfo['site'];
		$chips = isset($this->payments['price'][$site]) ? $this->payments['price'][$site][$price] : $this->payments['price']['default'][$price];		//商品
		if(!$chips) mdl_message::error(mdl_message::READ_CONFIG_ERROR);

		//本地下单
		$pid = mdl_payment_pay::getInstance()->place_order($mid, $product, $pmode, $price, $chips, $userInfo['chips']);
		$pid && mdl_payment_pay::getInstance()->change_update($pid, mdl_payment_pay::STATUS_THIRD_PLACE_ORDER);
		
		$pid = mdl_payment_query::getInstance()->getExternalPid($pid);
		mdl_message::success(mdl_message::SUCCESS, $pid);
	}

	//付款
	public function payment() {
		mdl_payment_pay::getInstance()->writelogs($_REQUEST);
		if(!isset($_REQUEST['anid']) || !isset($_REQUEST['package_id']) || !isset($_REQUEST['order_id']) || !isset($_REQUEST['total_amount']) ||!isset($_REQUEST['created_time'])){
			mdl_message::error(mdl_message::AUTH_PARAM_EMPTY);
		}
		$msg['anid'] = $_REQUEST['anid'];
		$msg['package_id'] = $_REQUEST['package_id'];
		$msg['order_id'] = $_REQUEST['order_id'];
		$msg['total_amount'] = $_REQUEST['total_amount'];
		$msg['created_time'] = $_REQUEST['created_time'];
		$msg['finished_time'] = $_REQUEST['finished_time'];
		$msg['partner_order_id'] = $_REQUEST['partner_order_id'];
		$msg['attach'] = $_REQUEST['attach'];
		$msg['secret'] = "8b12985a9c27bd4a9cbd67a10b011c21";
		//排序组合
		ksort($msg);
		$str = '';
		foreach($msg as $k=>$v) {
			if(!strlen($v)) return false;
			$str.= $k.'='.$v.'&';
		}
		$str = rtrim($str,'&');
		$msg['signature'] = $_REQUEST['signature'];
		$signature = md5($str);
		if($msg['signature'] != $signature)  mdl_message::error(mdl_message::PAY_SIGNATURE_ERROR); //签名有误
		$order = mdl_payment_pay::getInstance()->getOrderFrom($msg['partner_order_id']); //获取用户数据
		if($msg['total_amount']!=$order['price']) mdl_message::error(mdl_message::PAY_PRICE_ERROR); //比对价格
		$flag = mdl_payment_pay::getInstance()->complete($msg['partner_order_id'], $msg['order_id']);
		if($flag) {
			return 'success';
		} else {
			mdl_message::error(mdl_message::UNKNOWN);
		}
	}
	

}