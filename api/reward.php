<?php
class reward extends ujn_controller {
	var $auto_render = false;
//	检查登陆天数奖励筹码
	public function login() {
		$mid = !empty($_REQUEST['mid']) ? tls_func::uint($_REQUEST['mid']) : mdl_message::error(mdl_message::AUTH_USER_ERROR);

		$mtkey = !empty($_REQUEST['s']) ? tls_func::escape($_REQUEST['s']) : 0;
		$mtkey = mdl_member_info::getInstance()->get_uid_by_mtkey($mtkey,$mid);
		if(!$mtkey) return mdl_message::error(mdl_message::AUTH_MTKEY_ERROR);

		$state = mdl_logs_limit::getInstance()->limitOnceDaily($mid, mdl_logs_limit::COUNT_LOGIN_CHIPS,true);
		if($state) mdl_message::error(mdl_message::AUTH_ALREADY_REWARD,'已经领取'); //已经奖励过
			
		$userInfo = mdl_member_info::getInstance()->getUserByUID($mid); //获取用户信息
		if(empty($userInfo)) mdl_message::error(mdl_message::AUTH_USER_ERROR);  //没有这个用户,

		$lmode = mdl_logs_chips::CONT_LOGIN; //奖励方式

		$chipsArray = array(
			1 => 800,
			2 => 900,
			3 => 1000,
			4 => 1200,
			5 => 1500			
		);
		$indexs = 1;
		isset($chipsArray[$userInfo['series_times']]) && $index = $userInfo['series_times'];
		$userInfo['series_times'] > 5 && $index = 5;
		
		$chips = $chipsArray[$index];

		$flag = mdl_logs_chips::getInstance()->addWin($mid, mdl_logs_chips::CONT_LOGIN, mdl_logs_chips::FLAG_ADD, $chips, '','',1);
		if($flag) {
			mdl_logs_limit::getInstance()->limitOnceDaily($mid, mdl_logs_limit::COUNT_LOGIN_CHIPS, false);
			mdl_message::success(mdl_message::SUCCESS, $chips);
		} else {
			mdl_message::error(mdl_message::UNKNOWN);
		}
	}
}