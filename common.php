<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

$httpPrex = 'http://';
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
	$httpPrex = 'https://';
}
define('HTTP_PRE', $httpPrex);

define('PRIVATE_KEY',		'4befc16503e30b069ccce657608ac690');

define('PATH_ROOT',         dirname(__FILE__) . DIRECTORY_SEPARATOR);

$platform_flag = require_once PATH_ROOT . '_tmp/platform_flag.php';

define('PLATFORM', $platform_flag);

include_once PATH_ROOT . '../ujn2/ujn.php';
ujn_runtime::setSplitAlias('default');

define('PATH_MODELS', 				PATH_ROOT . 'models' . DS);
define('PATH_POINTS', 				PATH_ROOT . 'api' . DS);
define('PATH_TEMPLATE', 			PATH_ROOT . 'views' . DS);
define('PATH_TMP', 					PATH_ROOT . '_tmp' . DS);
define('PATH_CRONTAB',        PATH_ROOT . 'crontab' . DS);

define("YESTERDAY", TODAY - 86400);
class base{	
	//打印方法
	static public function p($data){
		if(is_array($data)){
			echo '<pre>';
			print_r($data);
		}else{
			var_dump($data);
		}
	}

	static public function error($message="输入有误,请重新输入",$url=null){
		echo '<script>alert("'.$message.'");</script>';
		if($s==null){
			echo '<script>window.history.go(-1)</script>';
		}else{
			echo '<script>window.location.href="'+$url+'";</script>';
		}
		die;
	}
}